## Environnement
Ce projet part du principe que l'utilisateur a **git**, **composer** et **docker** sur son ordinateur.  
Nous avons utilisé un serveur **PHP** avec une base de données **MySQL** et **rabbitMQ** en messages broker.

## Installation

#### Cloner le projet  
```
git clone https://gitlab.com/BastienHetic/architecture-v1.git
```  
***
#### Création des conteneurs docker  
```
docker-compose build
docker-compose up -d
```

***

#### Vérifier l'accès a rabbitmq management  
Si à l'url [localhost:15672](http://localhost:15672), vous arrivez sur une erreur 500, suivez les instructions ci-dessous :
- Ouvrir un terminal du conteneur architecture-v1_rabbitmq_1
- Exécuter la commande suivante : ``rabbitmq-plugins enable rabbitmq_management``

***

#### Ajouter les dépendances  
Exécuter les commandes suivantes :  

```
cd src
composer install
```  

Si une erreur survient lors de la commande ``composer install``, vérifier que l'extension socket n'est pas commenté dans le fichier php.ini de la version de **PHP** que vous utilisez :  ``extension=sockets``

## Utilisation

Pour ajouter un avatar se rendre à l'url [localhost:80](http://localhost:80) et se connecter avec les identifiants suivants :  
**Pseudo** : admin  
**Mot de passe** : admin

***

Pour consommer les messages en file d'attente, suivez les instructions ci-dessous :
- Ouvrir le terminal du conteneur **architecture-v1_php_1**
- Executer la commande suivante : ``php receive.php``