<?php

require __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$mysqli = new mysqli("db", "root", "example", "architectureV1");
$connection = new AMQPStreamConnection('rabbit1', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('avatar', false, false, false, false);

echo " [*] Waiting for messages. To exit press CTRL+C\n";

$callback = function ($data) use ($mysqli) {
    $msg = json_decode($data->body, true);

    if(!is_dir('avatar')){
        mkdir('avatar');
    }

    $file = fopen('avatar/' . $msg['link_avatar'], 'wb');
    $content = explode(',', $msg['base64']);
    fwrite($file, base64_decode($content[1]));
    fclose($file);

    list($width, $height) = getimagesize('avatar/' . $msg['link_avatar']);
    $ext = pathinfo($msg['link_avatar'], PATHINFO_EXTENSION);
    echo 'avatar/' . $msg['link_avatar'];
    if ($ext == 'png') {
        $src = imagecreatefrompng('avatar/' . $msg['link_avatar']);
    } else {
        $src = imagecreatefromjpeg('avatar/' . $msg['link_avatar']);
    }
    $dst = imagecreatetruecolor(100, 100);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, 100, 100, $width, $height);

    if ($ext == 'png' || $ext == 'PNG') {
        imagepng($dst, 'avatar/' . $msg['link_avatar']);
    } else {
        imagejpeg($dst, 'avatar/' . $msg['link_avatar']);
    }

    $mysqli->query(
        'INSERT INTO avatar (image_link, user_id)
        VALUES ("' . $msg['link_avatar'] . '", ' . $msg['user']['id'] . ') ON DUPLICATE KEY UPDATE image_link = "'. $msg['link_avatar'] .'"'
    );

};

$channel->basic_consume('avatar', '', false, true, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();