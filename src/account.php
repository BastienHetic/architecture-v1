<?php
require __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

session_start();

if (isset($_POST['disconnect'])) {
    unset($_SESSION['user']);
}

if (empty($_SESSION['user'])) {
    header('Location: http://localhost');
    exit;
}
$mysqli = new mysqli("db", "root", "example", "architectureV1");
$query = $mysqli->query('SELECT image_link FROM avatar WHERE user_id = ' . $_SESSION['user']['id'] . ' LIMIT 1');
if ($query->num_rows == 1) {
    $image = $query->fetch_array(MYSQLI_ASSOC);
} else {
    $image = null;
}

$msg = null;
if (isset($_FILES['avatar']) && empty($_FILES['avatar']['error'])) {
    $connection = new AMQPStreamConnection('rabbit1', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $channel->queue_declare('avatar', false, false, false, false);

    $ext = pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);
    $base64 = 'data:image/' . $ext . ';base64,' . base64_encode(file_get_contents($_FILES['avatar']['tmp_name']));
    $msg = new AMQPMessage(json_encode([
        'user' => $_SESSION['user'],
        'link_avatar' => $_SESSION['user']['username'] . '.' . $ext,
        'base64' => $base64
    ]), ['content_type' => 'application/json']);
    $channel->basic_publish($msg, '', 'avatar');

    $msg = "Votre demande de changement d'avatar a été pris en compte et sera traité dans les meilleurs délais";

    $channel->close();
    $connection->close();
}
?>

<html>

<body>
    <h2>Bonjour <?= $_SESSION['user']['username'] ?></h2>
    <?php if (!empty($image)) { ?>
        <img src="./avatar/<?= $image['image_link'] ?>">
    <?php } ?>
    <?php if (!empty($msg)) { ?>
        <p><?= $msg ?></p>
    <?php } ?>

    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="avatar" />
        <button type="submit">Changer mon avatar</button>
    </form>
    <form method="POST">
        <input type="hidden" name="disconnect" value="1" />
        <button type="submit">Déconnexion</button>
    </form>
</body>

</html>